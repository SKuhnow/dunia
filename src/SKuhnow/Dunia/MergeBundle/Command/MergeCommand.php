<?php
namespace SKuhnow\Dunia\MergeBundle\Command;

use SKuhnow\Dunia\Merge\Service\MergerFactory;
use SKuhnow\Dunia\Merge\Service\MergeSaver;
use SKuhnow\Dunia\MergeBundle\Services\MergeService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MergeCommand extends ContainerAwareCommand
{

    /**
     * @var \SKuhnow\Dunia\MergeBundle\Services\MergeService
     */
    private $mergeService;

    protected function configure()
    {
        $this
            ->setName('dunia:merge')
            ->setDescription('Merge something')
            ->addArgument(
                'duplicateGroup',
                InputArgument::REQUIRED,
                'Which duplicate group do you want to merge today?'
            );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->mergeService = $this->getContainer()->get('dunia.merger');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $duplicateGroupId = $input->getArgument('duplicateGroup');
        $this->mergeService->merge($duplicateGroupId);
    }

}
