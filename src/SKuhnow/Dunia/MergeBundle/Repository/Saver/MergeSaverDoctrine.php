<?php
namespace SKuhnow\Dunia\MergeBundle\Repository\Saver;

use Doctrine\ORM\EntityManager;
use SKuhnow\Dunia\Merge\AbstractMergeResultSaver;

class MergeSaverDoctrine extends AbstractMergeResultSaver
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;

    }

    protected function saveEntity($entity)
    {
        // nothing - entities are already updated
    }

    protected function deleteEntity($entity)
    {
        $this->em->remove($entity);
    }

    protected function afterProcessing()
    {
        $this->em->flush();
    }

}
