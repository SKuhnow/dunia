<?php
namespace SKuhnow\Dunia\MergeBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Console as ConsoleBundle;
use \Symfony\Component\Console as ConsoleComponent;

class BaseTestCase extends KernelTestCase
{

    /**
     * @var ConsoleBundle\Application
     */
    private $_application;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $_container;

    public function __construct()
    {
        self::bootKernel();
        $this->_container = self::$kernel->getContainer();
        $this->_application = new ConsoleBundle\Application(self::$kernel);
        $this->_application->setAutoExit(false);
        $this->_application->setCatchExceptions(false);
        parent::__construct();
    }

    protected function getService($serviceId)
    {
        return $this->_container->get($serviceId);
    }

    protected function getContainer()
    {
        return $this->_container;
    }

    protected function runConsole($command, Array $options = array())
    {
        $options["-e"] = "test";
        $options["-q"] = null;
        $options["-n"] = null;
        $options = array_merge($options, array('command' => $command));
        return $this->_application->run(new ConsoleComponent\Input\ArrayInput($options));
    }

    protected function recreateDatabaseWithFixtures($directory = '')
    {
        $this->runConsole("doctrine:schema:drop", array("--force" => true));
        $this->runConsole("doctrine:schema:create");

        if ($directory == '') {
            if (is_dir(__DIR__ . "/../DataFixtures")) {
                $directory = realpath(__DIR__ . "/../DataFixtures");
            } else {
                return;
            }
        } else {
            if (!is_dir($directory)) {
                throw new \Exception($directory . ' does not exists!');
            }
        }
        $directory = realpath($directory);
        $this->runConsole("doctrine:fixtures:load", array("--fixtures" => $directory));
    }

    protected function appendFixtures($directory)
    {
        $this->runConsole(
            "doctrine:fixtures:load",
            array(
                "--fixtures" => $directory,
                "--append" => null,
            )
        );
    }
}
