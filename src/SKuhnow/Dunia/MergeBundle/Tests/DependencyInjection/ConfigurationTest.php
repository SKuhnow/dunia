<?php
namespace SKuhnow\Dunia\MergeBundle\Tests\DependencyInjection;

use Matthias\SymfonyConfigTest\PhpUnit\AbstractConfigurationTestCase;
use SKuhnow\Dunia\MergeBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Yaml\Yaml;

class ConfigurationTest extends AbstractConfigurationTestCase
{

    public function testValidComplete()
    {
        $content = file_get_contents(__DIR__ . '/Configuration.yml');
        $configValues = Yaml::parse($content);

        $configuration = new Configuration();
        $processor = new Processor();
        $processor->processConfiguration($configuration, $configValues);
    }

    protected function getConfiguration()
    {
        return new Configuration();
    }
}
