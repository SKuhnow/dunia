<?php
namespace SKuhnow\Dunia\MergeBundle\Services;

use SKuhnow\Dunia\Merge\AbstractMergeResultSaver;
use SKuhnow\Dunia\MergeBundle\Merger\Factory\MergerFactory;

class MergeService
{

    /**
     * @var MergerFactory
     */
    private $mergerFactory;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repositoryDuplicateGroup;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repositoryPerson;

    /**
     * @var array
     */
    private $globalMergerConfig;

    /**
     * @var \SKuhnow\Dunia\Merge\MergeSaver
     */
    private $mergeSaver;

    public function __construct(
        AbstractMergeResultSaver $mergeSaver,
        MergerFactory $mergerFactory,
        $globalMergerConfig,
        $doctrine
    ) {
        $this->mergerFactory = $mergerFactory;
        $this->mergeSaver = $mergeSaver;
        $this->globalMergerConfig = $globalMergerConfig;

        $this->repositoryPerson = $doctrine->getRepository('SKuhnowDuniaPersonBundle:NaturalPerson');
        $this->repositoryDuplicateGroup = $doctrine->getRepository('SKuhnowDuniaMergeBundle:DuplicateGroupHead');
    }

    public function merge($duplicateGroupId)
    {
        $duplicateGroup = $this->getDuplicateGroup($duplicateGroupId);

        $entities = array();
        foreach ($duplicateGroup->getDuplicateGroupElements() as $duplicateGroupElement) {
            $entityId = $duplicateGroupElement->getEntityId();
            $entities[] = $this->repositoryPerson->find($entityId);
        }
        $merger = $this->mergerFactory->create($this->globalMergerConfig);
        $mergeResults = $merger->merge('PersonComplete', $entities, array(), array());
        $this->mergeSaver->saveMergeResults($mergeResults);
    }

    /**
     * @param $duplicateGroupId
     * @return \SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupHead
     */
    protected function getDuplicateGroup($duplicateGroupId)
    {
        $duplicateGroup = $this->repositoryDuplicateGroup->find($duplicateGroupId);
        return $duplicateGroup;
    }
}