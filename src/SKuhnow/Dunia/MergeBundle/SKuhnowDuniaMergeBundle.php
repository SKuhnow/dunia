<?php

namespace SKuhnow\Dunia\MergeBundle;

use SKuhnow\Dunia\MergeBundle\DependencyInjection\DuniaMergeExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class SKuhnowDuniaMergeBundle extends Bundle
{

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        // register extensions that do not follow the conventions manually
        $container->registerExtension(new DuniaMergeExtension());
    }
}
