<?php

namespace SKuhnow\Dunia\MergeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DuplicateGroupHead
 */
class DuplicateGroupHead
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $mergeSet;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $duplicateGroupElements;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->duplicateGroupElements = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mergeSet
     *
     * @param string $mergeSet
     * @return DuplicateGroupHead
     */
    public function setMergeSet($mergeSet)
    {
        $this->mergeSet = $mergeSet;

        return $this;
    }

    /**
     * Get mergeSet
     *
     * @return string 
     */
    public function getMergeSet()
    {
        return $this->mergeSet;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return DuplicateGroupHead
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add duplicateGroupElements
     *
     * @param \SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupElement $duplicateGroupElements
     * @return DuplicateGroupHead
     */
    public function addDuplicateGroupElement(\SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupElement $duplicateGroupElements)
    {
        $this->duplicateGroupElements[] = $duplicateGroupElements;

        return $this;
    }

    /**
     * Remove duplicateGroupElements
     *
     * @param \SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupElement $duplicateGroupElements
     */
    public function removeDuplicateGroupElement(\SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupElement $duplicateGroupElements)
    {
        $this->duplicateGroupElements->removeElement($duplicateGroupElements);
    }

    /**
     * Get duplicateGroupElements
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDuplicateGroupElements()
    {
        return $this->duplicateGroupElements;
    }
}
