<?php

namespace SKuhnow\Dunia\MergeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DuplicateGroupElement
 */
class DuplicateGroupElement
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $duplicateGroupElementId;

    /**
     * @var string
     */
    private $entityId;

    /**
     * @var \SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupHead
     */
    private $duplicateGroupHead;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set duplicateGroupElementId
     *
     * @param integer $duplicateGroupElementId
     * @return DuplicateGroupElement
     */
    public function setDuplicateGroupElementId($duplicateGroupElementId)
    {
        $this->duplicateGroupElementId = $duplicateGroupElementId;

        return $this;
    }

    /**
     * Get duplicateGroupElementId
     *
     * @return integer 
     */
    public function getDuplicateGroupElementId()
    {
        return $this->duplicateGroupElementId;
    }

    /**
     * Set entityId
     *
     * @param string $entityId
     * @return DuplicateGroupElement
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;

        return $this;
    }

    /**
     * Get entityId
     *
     * @return string 
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * Set duplicateGroupHead
     *
     * @param \SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupHead $duplicateGroupHead
     * @return DuplicateGroupElement
     */
    public function setDuplicateGroupHead(\SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupHead $duplicateGroupHead = null)
    {
        $this->duplicateGroupHead = $duplicateGroupHead;

        return $this;
    }

    /**
     * Get duplicateGroupHead
     *
     * @return \SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupHead 
     */
    public function getDuplicateGroupHead()
    {
        return $this->duplicateGroupHead;
    }
}
