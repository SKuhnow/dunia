<?php

namespace SKuhnow\Dunia\MergeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dunia_merge');

        $rootNode
            ->children()
                ->scalarNode('default_rule_class')->end()
                ->arrayNode('entity_configuration')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('entity_classname')->isRequired()->cannotBeEmpty()->end()
                            ->arrayNode('fields')
                                ->isRequired()
                                ->requiresAtLeastOneElement()
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('deviating_getter')->cannotBeEmpty()->end()
                                        ->scalarNode('deviating_setter')->cannotBeEmpty()->end()
                                        ->scalarNode('rule_classname')->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('parents')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('deviating_getter')->cannotBeEmpty()->end()
                                        ->scalarNode('deviating_setter')->cannotBeEmpty()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('merge_set')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('entity_configuration')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->scalarNode('getter')->end()
                            // recursive
                            ->arrayNode('child_merge_sets')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('entity_configuration')
                                            ->isRequired()
                                            ->cannotBeEmpty()
                                        ->end()
                                        ->scalarNode('getter')->end()
                                        ->arrayNode('grouper')
                                            ->isRequired()
                                            ->cannotBeEmpty()
                                            ->children()
                                                ->scalarNode('classname')->end()
                                                ->scalarNode('fieldname')->end()
                                            ->end()
                                        ->end()
                                        ->arrayNode('child_merge_sets')
                                            ->prototype('array')
                                                ->children()
                                                    ->scalarNode('entity_configuration')
                                                        ->isRequired()
                                                        ->cannotBeEmpty()
                                                        ->end()
                                                    ->scalarNode('getter')->end()
                                                    ->arrayNode('grouper')
                                                        ->isRequired()
                                                        ->cannotBeEmpty()
                                                        ->children()
                                                            ->scalarNode('classname')->end()
                                                            ->scalarNode('fieldname')->end()
                                                        ->end()
                                                    ->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
