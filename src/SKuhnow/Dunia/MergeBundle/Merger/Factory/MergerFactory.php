<?php
namespace SKuhnow\Dunia\MergeBundle\Merger\Factory;

use SKuhnow\Dunia\Merge\Configuration;
use SKuhnow\Dunia\Merge\Merger;

class MergerFactory
{

    /**
     * @param $globalMergerConfiguration
     * @return Merger
     */
    public function create($globalMergerConfiguration)
    {
        $mergerConfiguration = new Configuration\Merger();

        $entityConfigurationsArray = $globalMergerConfiguration['entity_configuration'];
        $mergeSetsArray = $globalMergerConfiguration['merge_set'];

        // add entity-configurations
        foreach ($entityConfigurationsArray as $entityConfigurationName => $entityConfigurationArray) {
            $entityClassname = $entityConfigurationArray['entity_classname'];
            $entityFields = $entityConfigurationArray['fields'];
            $entityParents = $entityConfigurationArray['parents'];

            $entityConfiguration = $mergerConfiguration->newEntityConfiguration($entityClassname, $entityConfigurationName);
            $this->processEntityFields($entityConfiguration, $entityFields);
            $this->processParents($entityConfiguration, $entityParents);
        }

        // add merge-sets
        foreach ($mergeSetsArray as $mergeSetName => $mergeSetConfig) {
            $entityConfigurationName = $mergeSetConfig['entity_configuration'];
            $childMergeSets = $mergeSetConfig['child_merge_sets'];
            $mergeSet = $mergerConfiguration->newMergeSet($entityConfigurationName, $mergeSetName);

            $this->processChildMergeSetsRecursive($mergeSet, $childMergeSets);
        }

        return new Merger($mergerConfiguration);
    }

    private function processChildMergeSetsRecursive(Configuration\MergeSet $mergeSet, $childMergeSetConfigs)
    {
        foreach ($childMergeSetConfigs as $childMergeSetConfig) {
            $entityConfigurationName = $childMergeSetConfig['entity_configuration'];

            $getter = '';
            if (array_key_exists('getter', $childMergeSetConfig)) {
                $getter = $childMergeSetConfig['getter'];
            }
            $grouper = $this->instantiateGrouper($childMergeSetConfig['grouper']);

            $childMergeSet = $mergeSet->newChildMergeSet($entityConfigurationName, $grouper, $getter);

            if (array_key_exists('child_merge_sets', $childMergeSetConfig) && count($childMergeSetConfig['child_merge_sets']) > 0) {
                return $this->processChildMergeSetsRecursive($childMergeSet, $childMergeSetConfig['child_merge_sets']);
            }
        }

        return $mergeSet;
    }

    /**
     * @param $classname The classname of the rule
     * @return mixed
     * @throws \Exception
     */
    private function instantiateRule($classname)
    {
        if (class_exists($classname)) {
            return new $classname();
        } else {
            throw new \Exception(sprintf('Rule %s does not exists!', $classname));
        }
    }

    /**
     * @param Configuration\Entity $entityConfiguration
     * @param array $entityFields
     * @throws \Exception
     */
    private function processEntityFields(Configuration\Entity $entityConfiguration, array $entityFields)
    {
        foreach ($entityFields as $name => $entityFieldconfig) {
            $ruleClassname = $entityFieldconfig['rule_classname'];
            $rule = $this->instantiateRule($ruleClassname);
            $deviatingGetter = '';
            if (array_key_exists('deviating_getter', $entityFieldconfig)) {
                $deviatingGetter = $entityFieldconfig['deviating_getter'];
            }
            $deviatingSetter = '';
            if (array_key_exists('deviating_setter', $entityFieldconfig)) {
                $deviatingSetter = $entityFieldconfig['deviating_setter'];
            }
            $entityConfiguration->newEntityField($name, $rule, $deviatingGetter, $deviatingSetter);
        }
    }

    /**
     * @param Configuration\Entity $entityConfiguration
     * @param array $entityParents
     * @throws \Exception
     */
    private function processParents(Configuration\Entity $entityConfiguration, array $entityParents)
    {
        foreach ($entityParents as $parentEntityConfigurationName => $parentConfig) {
            $deviatingGetter = '';
            if (array_key_exists('deviating_getter', $parentConfig)) {
                $deviatingGetter = $parentConfig['deviating_getter'];
            }
            $deviatingSetter = '';
            if (array_key_exists('deviating_setter', $parentConfig)) {
                $deviatingSetter = $parentConfig['deviating_setter'];
            }
            $entityConfiguration->addParent($parentEntityConfigurationName, $deviatingGetter, $deviatingSetter);
        }
    }

    /**
     * @param array $grouperConfig
     * @return mixed
     * @throws \Exception
     */
    private function instantiateGrouper(array $grouperConfig)
    {
        $grouperClassname = $grouperConfig['classname'];
        $parameter = $grouperConfig['fieldname'];

        if (class_exists($grouperClassname)) {
            return new $grouperClassname($parameter);
        } else {
            throw new \Exception(sprintf('Grouper %s does not exists!', $grouperClassname));
        }
    }
}
