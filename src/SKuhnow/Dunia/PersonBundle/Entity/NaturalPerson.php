<?php

namespace SKuhnow\Dunia\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NaturalPerson
 */
class NaturalPerson
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $salutation;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $nobility;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $surname;

    /**
     * @var integer
     */
    private $alter;

    /**
     * @var integer
     */
    private $schadensfreieJahre;

    /**
     * @var \DateTime
     */
    private $geburtstag;

    /**
     * @var \DateTime
     */
    private $anmeldedatum;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vehicles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set salutation
     *
     * @param string $salutation
     * @return NaturalPerson
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get salutation
     *
     * @return string 
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return NaturalPerson
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set nobility
     *
     * @param string $nobility
     * @return NaturalPerson
     */
    public function setNobility($nobility)
    {
        $this->nobility = $nobility;

        return $this;
    }

    /**
     * Get nobility
     *
     * @return string 
     */
    public function getNobility()
    {
        return $this->nobility;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return NaturalPerson
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return NaturalPerson
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set alter
     *
     * @param integer $alter
     * @return NaturalPerson
     */
    public function setAlter($alter)
    {
        $this->alter = $alter;

        return $this;
    }

    /**
     * Get alter
     *
     * @return integer 
     */
    public function getAlter()
    {
        return $this->alter;
    }

    /**
     * Set schadensfreieJahre
     *
     * @param integer $schadensfreieJahre
     * @return NaturalPerson
     */
    public function setSchadensfreieJahre($schadensfreieJahre)
    {
        $this->schadensfreieJahre = $schadensfreieJahre;

        return $this;
    }

    /**
     * Get schadensfreieJahre
     *
     * @return integer 
     */
    public function getSchadensfreieJahre()
    {
        return $this->schadensfreieJahre;
    }

    /**
     * Set geburtstag
     *
     * @param \DateTime $geburtstag
     * @return NaturalPerson
     */
    public function setGeburtstag($geburtstag)
    {
        $this->geburtstag = $geburtstag;

        return $this;
    }

    /**
     * Get geburtstag
     *
     * @return \DateTime 
     */
    public function getGeburtstag()
    {
        return $this->geburtstag;
    }

    /**
     * Set anmeldedatum
     *
     * @param \DateTime $anmeldedatum
     * @return NaturalPerson
     */
    public function setAnmeldedatum($anmeldedatum)
    {
        $this->anmeldedatum = $anmeldedatum;

        return $this;
    }

    /**
     * Get anmeldedatum
     *
     * @return \DateTime 
     */
    public function getAnmeldedatum()
    {
        return $this->anmeldedatum;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return NaturalPerson
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return NaturalPerson
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return NaturalPerson
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add vehicles
     *
     * @param \SKuhnow\Dunia\PersonBundle\Entity\Vehicle $vehicles
     * @return NaturalPerson
     */
    public function addVehicle(\SKuhnow\Dunia\PersonBundle\Entity\Vehicle $vehicles)
    {
        $this->vehicles[] = $vehicles;

        return $this;
    }

    /**
     * Remove vehicles
     *
     * @param \SKuhnow\Dunia\PersonBundle\Entity\Vehicle $vehicles
     */
    public function removeVehicle(\SKuhnow\Dunia\PersonBundle\Entity\Vehicle $vehicles)
    {
        $this->vehicles->removeElement($vehicles);
    }

    /**
     * Get vehicles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }
}
