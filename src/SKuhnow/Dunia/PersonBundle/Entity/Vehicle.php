<?php

namespace SKuhnow\Dunia\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehicle
 */
class Vehicle
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $manufacturer;

    /**
     * @var integer
     */
    private $modelYear;

    /**
     * @var string
     */
    private $modelKey;

    /**
     * @var string
     */
    private $vehicleIdentificationNumber;

    /**
     * @var integer
     */
    private $odometer;

    /**
     * @var \DateTime
     */
    private $generalInspectionAt;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var string
     */
    private $createdBy;

    /**
     * @var \DateTime
     */
    private $deletedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vehicleAttributes;

    /**
     * @var \SKuhnow\Dunia\PersonBundle\Entity\NaturalPerson
     */
    private $naturalPerson;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vehicleAttributes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     * @return Vehicle
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set modelYear
     *
     * @param integer $modelYear
     * @return Vehicle
     */
    public function setModelYear($modelYear)
    {
        $this->modelYear = $modelYear;

        return $this;
    }

    /**
     * Get modelYear
     *
     * @return integer 
     */
    public function getModelYear()
    {
        return $this->modelYear;
    }

    /**
     * Set modelKey
     *
     * @param string $modelKey
     * @return Vehicle
     */
    public function setModelKey($modelKey)
    {
        $this->modelKey = $modelKey;

        return $this;
    }

    /**
     * Get modelKey
     *
     * @return string 
     */
    public function getModelKey()
    {
        return $this->modelKey;
    }

    /**
     * Set vehicleIdentificationNumber
     *
     * @param string $vehicleIdentificationNumber
     * @return Vehicle
     */
    public function setVehicleIdentificationNumber($vehicleIdentificationNumber)
    {
        $this->vehicleIdentificationNumber = $vehicleIdentificationNumber;

        return $this;
    }

    /**
     * Get vehicleIdentificationNumber
     *
     * @return string 
     */
    public function getVehicleIdentificationNumber()
    {
        return $this->vehicleIdentificationNumber;
    }

    /**
     * Set odometer
     *
     * @param integer $odometer
     * @return Vehicle
     */
    public function setOdometer($odometer)
    {
        $this->odometer = $odometer;

        return $this;
    }

    /**
     * Get odometer
     *
     * @return integer 
     */
    public function getOdometer()
    {
        return $this->odometer;
    }

    /**
     * Set generalInspectionAt
     *
     * @param \DateTime $generalInspectionAt
     * @return Vehicle
     */
    public function setGeneralInspectionAt($generalInspectionAt)
    {
        $this->generalInspectionAt = $generalInspectionAt;

        return $this;
    }

    /**
     * Get generalInspectionAt
     *
     * @return \DateTime 
     */
    public function getGeneralInspectionAt()
    {
        return $this->generalInspectionAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Vehicle
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param string $createdBy
     * @return Vehicle
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return string 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Vehicle
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Add vehicleAttributes
     *
     * @param \SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute $vehicleAttributes
     * @return Vehicle
     */
    public function addVehicleAttribute(\SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute $vehicleAttributes)
    {
        $this->vehicleAttributes[] = $vehicleAttributes;

        return $this;
    }

    /**
     * Remove vehicleAttributes
     *
     * @param \SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute $vehicleAttributes
     */
    public function removeVehicleAttribute(\SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute $vehicleAttributes)
    {
        $this->vehicleAttributes->removeElement($vehicleAttributes);
    }

    /**
     * Get vehicleAttributes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVehicleAttributes()
    {
        return $this->vehicleAttributes;
    }

    /**
     * Set naturalPerson
     *
     * @param \SKuhnow\Dunia\PersonBundle\Entity\NaturalPerson $naturalPerson
     * @return Vehicle
     */
    public function setNaturalPerson(\SKuhnow\Dunia\PersonBundle\Entity\NaturalPerson $naturalPerson = null)
    {
        $this->naturalPerson = $naturalPerson;

        return $this;
    }

    /**
     * Get naturalPerson
     *
     * @return \SKuhnow\Dunia\PersonBundle\Entity\NaturalPerson 
     */
    public function getNaturalPerson()
    {
        return $this->naturalPerson;
    }
}
