<?php

namespace SKuhnow\Dunia\PersonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VehicleAttribute
 */
class VehicleAttribute
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $vehicleId;

    /**
     * @var string
     */
    private $attributeKey;

    /**
     * @var string
     */
    private $attributeValue;

    /**
     * @var \SKuhnow\Dunia\PersonBundle\Entity\Vehicle
     */
    private $vehicle;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicleId
     *
     * @param string $vehicleId
     * @return VehicleAttribute
     */
    public function setVehicleId($vehicleId)
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    /**
     * Get vehicleId
     *
     * @return string 
     */
    public function getVehicleId()
    {
        return $this->vehicleId;
    }

    /**
     * Set attributeKey
     *
     * @param string $attributeKey
     * @return VehicleAttribute
     */
    public function setAttributeKey($attributeKey)
    {
        $this->attributeKey = $attributeKey;

        return $this;
    }

    /**
     * Get attributeKey
     *
     * @return string 
     */
    public function getAttributeKey()
    {
        return $this->attributeKey;
    }

    /**
     * Set attributeValue
     *
     * @param string $attributeValue
     * @return VehicleAttribute
     */
    public function setAttributeValue($attributeValue)
    {
        $this->attributeValue = $attributeValue;

        return $this;
    }

    /**
     * Get attributeValue
     *
     * @return string 
     */
    public function getAttributeValue()
    {
        return $this->attributeValue;
    }

    /**
     * Set vehicle
     *
     * @param \SKuhnow\Dunia\PersonBundle\Entity\Vehicle $vehicle
     * @return VehicleAttribute
     */
    public function setVehicle(\SKuhnow\Dunia\PersonBundle\Entity\Vehicle $vehicle = null)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return \SKuhnow\Dunia\PersonBundle\Entity\Vehicle 
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }
}
