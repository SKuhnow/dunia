<?php
namespace SKuhnow\Dunia\PersonBundle\Merger\Rules;

use SKuhnow\Dunia\MergeBundle\Merger\Rules\GreaterRule;
use SKuhnow\Dunia\MergeBundle\Merger\Rules\Interfaces\MergeRuleInstance;
use SKuhnow\Dunia\MergeBundle\Merger\Rules\OlderDateRule;
use SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute;

class VehicleAttributeRule implements MergeRuleInstance
{

    /**
     * @param $vehicleAttributes
     * @return bool|mixed
     * @throws \Exception
     */
    public function decide($vehicleAttributes)
    {
        if (!is_array($vehicleAttributes) || count($vehicleAttributes) == 0) {
            throw new \Exception('Not an array or an empty array');
        }
        foreach ($vehicleAttributes as $vehicleAttribute) {
            if (!$vehicleAttribute instanceof VehicleAttribute) {
                $isInstanceOf = get_class($vehicleAttribute);
                throw new \Exception(
                    'Must be instance of SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute, but is instance of ' .
                    $isInstanceOf
                );
            }
        }
        $key = $vehicleAttributes[0]->getAttributeKey();

        switch ($key) {
            case 'bonus':
                return $this->decideForBonus($vehicleAttributes);
            default:
                return $this->takeGreatesValue($vehicleAttributes);
        }
    }

    /**
     * @param $vehicleAttributes
     * @return bool
     */
    private function decideForBonus($vehicleAttributes)
    {
        /**
         * @var $vehicleAttribute \SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute
         */
        foreach ($vehicleAttributes as $vehicleAttribute) {
            $value = $vehicleAttribute->getAttributeValue();
            if ($value == '1') {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $vehicleAttributes
     * @return mixed
     */
    private function takeGreatesValue($vehicleAttributes)
    {
        $values = array();
        foreach ($vehicleAttributes as $vehicleAttribute) {
            $values[] = $vehicleAttribute->getAttributeValue();
        }
        $rule = new GreaterRule();
        return $rule->decide($values);
    }
}
