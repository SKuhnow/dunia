<?php
namespace SKuhnow\Dunia\PersonBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupElement;
use SKuhnow\Dunia\MergeBundle\Entity\DuplicateGroupHead;
use SKuhnow\Dunia\PersonBundle\Entity\NaturalPerson;
use SKuhnow\Dunia\PersonBundle\Entity\Vehicle;
use SKuhnow\Dunia\PersonBundle\Entity\VehicleAttribute;

class LoadPersonData implements FixtureInterface
{

    /**
     *
     * @param ObjectManager $manager
     * @return DuplicateGroupHead
     */
    public function load(ObjectManager $manager)
    {
        $person = new NaturalPerson();
        $person->setSalutation('Herr');
        $person->setFirstname('Hans');
        $person->setSurname('Meiser');
        $person->setAlter(50);
        $person->setAnmeldedatum(new \DateTime('2015-01-13'));
        $person->setGeburtstag(new \DateTime('1965-01-06'));
        $person->setSchadensfreieJahre(7);
        $person->setCreatedAt(new \DateTime());
        $person->setCreatedBy('Fixtures');
        $manager->persist($person);

        $vehicle = new Vehicle();
        $vehicle->setManufacturer('Audi');
        $vehicle->setModelYear(2012);
        $vehicle->setModelKey('ABC123');
        $vehicle->setVehicleIdentificationNumber('WAUZZZ00000000001');
        $vehicle->setOdometer(12364);
        $vehicle->setGeneralInspectionAt(new \DateTime('2017-05-01'));
        $vehicle->setCreatedAt(new \DateTime());
        $vehicle->setCreatedBy('Fixtures');
        $vehicle->setNaturalPerson($person);
        $person->addVehicle($vehicle);
        $manager->persist($vehicle);

        $vehicleAttribute = new VehicleAttribute();
        $vehicleAttribute->setAttributeKey('foo date');
        $vehicleAttribute->setAttributeValue(date('Y-m-d') . ' 15:23:30');
        $vehicleAttribute->setVehicle($vehicle);
        $vehicle->addVehicleAttribute($vehicleAttribute);
        $manager->persist($vehicleAttribute);

        $vehicleAttributeBonus = new VehicleAttribute();
        $vehicleAttributeBonus->setAttributeKey('bonus');
        $vehicleAttributeBonus->setAttributeValue('1');
        $vehicleAttributeBonus->setVehicle($vehicle);
        $vehicle->addVehicleAttribute($vehicleAttributeBonus);
        $manager->persist($vehicleAttributeBonus);

        $person2 = new NaturalPerson();
        $person2->setSalutation('Herr');
        $person2->setFirstname('Hans');
        $person2->setSurname('Meiser');
        $person2->setAlter(51);
        $person2->setAnmeldedatum(new \DateTime('2015-02-13'));
        $person2->setGeburtstag(new \DateTime('1965-01-05'));
        $person2->setSchadensfreieJahre(9);
        $person2->setCreatedAt(new \DateTime());
        $person2->setCreatedBy('Fixtures');
        $manager->persist($person2);

        $vehicle2 = new Vehicle();
        $vehicle2->setManufacturer('Audi');
        $vehicle2->setModelYear(2012);
        $vehicle2->setModelKey('ABC123');
        $vehicle2->setVehicleIdentificationNumber('WAUZZZ00000000001');
        $vehicle2->setOdometer(24654);
        $vehicle2->setGeneralInspectionAt(new \DateTime('2015-05-01'));
        $vehicle2->setCreatedAt(new \DateTime());
        $vehicle2->setCreatedBy('Fixtures');
        $vehicle2->setNaturalPerson($person2);
        $person2->addVehicle($vehicle2);
        $manager->persist($vehicle2);

        $vehicleAttribute2 = new VehicleAttribute();
        $vehicleAttribute2->setAttributeKey('foo date');
        $vehicleAttribute2->setAttributeValue(date('Y-m-d') . ' 16:30:19');
        $vehicleAttribute2->setVehicle($vehicle2);
        $vehicle2->addVehicleAttribute($vehicleAttribute2);
        $manager->persist($vehicleAttribute2);

        $vehicleAttributeBonus2 = new VehicleAttribute();
        $vehicleAttributeBonus2->setAttributeKey('bonus');
        $vehicleAttributeBonus2->setAttributeValue('0');
        $vehicleAttributeBonus2->setVehicle($vehicle2);
        $vehicle2->addVehicleAttribute($vehicleAttributeBonus2);
        $manager->persist($vehicleAttributeBonus2);

        $vehicle3 = new Vehicle();
        $vehicle3->setManufacturer('Audi');
        $vehicle3->setModelYear(2015);
        $vehicle3->setModelKey('ABC123');
        $vehicle3->setVehicleIdentificationNumber('WAUZZZ00000000002');
        $vehicle3->setOdometer(12364);
        $vehicle3->setCreatedAt(new \DateTime());
        $vehicle3->setCreatedBy('Fixtures');
        $vehicle3->setNaturalPerson($person2);
        $person2->addVehicle($vehicle3);
        $manager->persist($vehicle3);

        $vehicleAttribute3 = new VehicleAttribute();
        $vehicleAttribute3->setAttributeKey('foo date');
        $vehicleAttribute3->setAttributeValue(date('Y-m-d') . ' 23:30:19');
        $vehicleAttribute3->setVehicle($vehicle3);
        $vehicle3->addVehicleAttribute($vehicleAttribute3);
        $manager->persist($vehicleAttribute3);

        $person3 = new NaturalPerson();
        $person3->setSalutation('Herr');
        $person3->setFirstname('Hans');
        $person3->setSurname('Meiser');
        $person3->setAlter(48);
        $person3->setAnmeldedatum(new \DateTime('2014-02-13'));
        $person3->setGeburtstag(new \DateTime('1969-01-05'));
        $person3->setSchadensfreieJahre(15);
        $person3->setCreatedAt(new \DateTime());
        $person3->setCreatedBy('Fixtures');
        $manager->persist($person3);

        $duplicateGroupHead = new DuplicateGroupHead();
        $duplicateGroupHead->setMergeSet('Person');
        $duplicateGroupHead->setCreatedAt(new \DateTime());
        $manager->persist($duplicateGroupHead);

        $duplicateGroupElement1 = new DuplicateGroupElement();
        $duplicateGroupElement1->setEntityId($person->getId());
        $duplicateGroupElement1->setDuplicateGroupHead($duplicateGroupHead);
        $duplicateGroupHead->addDuplicateGroupElement(($duplicateGroupElement1));
        $manager->persist($duplicateGroupElement1);

        $duplicateGroupElement2 = new DuplicateGroupElement();
        $duplicateGroupElement2->setEntityId($person2->getId());
        $duplicateGroupElement2->setDuplicateGroupHead($duplicateGroupHead);
        $duplicateGroupHead->addDuplicateGroupElement(($duplicateGroupElement2));
        $manager->persist($duplicateGroupElement2);

        $duplicateGroupElement3 = new DuplicateGroupElement();
        $duplicateGroupElement3->setEntityId($person3->getId());
        $duplicateGroupElement3->setDuplicateGroupHead($duplicateGroupHead);
        $duplicateGroupHead->addDuplicateGroupElement(($duplicateGroupElement3));
        $manager->persist($duplicateGroupElement3);

        $manager->flush();

        return $duplicateGroupHead;
    }
}
